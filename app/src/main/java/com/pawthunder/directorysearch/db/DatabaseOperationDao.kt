package com.pawthunder.directorysearch.db

import androidx.room.*

/**
 * Base operations for every dao manipulating entities in database
 * @author Peter Grajko
 * @since 1.0
 * @param <T> type of entity
 */
@Dao
interface DatabaseOperationDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(item: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItems(itemList: List<T>)

    @Update
    fun updateItem(item: T)

    @Update
    fun updateItems(items: List<T>)

    @Delete
    fun deleteItem(item: T)

    @Delete
    fun deleteItems(items: List<T>)
}