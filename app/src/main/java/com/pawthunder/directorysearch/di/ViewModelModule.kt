package com.pawthunder.directorysearch.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pawthunder.directorysearch.ui.files.FilesViewModel
import com.pawthunder.directorysearch.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Module for viewModels bound to lifecycle objects
 * @author Peter Grajko
 * @since 1.0
 */
@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(FilesViewModel::class)
    abstract fun bindPortfolioViewModel(portfolioModel: FilesViewModel): ViewModel

}
