package com.pawthunder.directorysearch.ui.files

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.pawthunder.directorysearch.R
import com.pawthunder.directorysearch.databinding.ItemFolderDialogBinding
import com.pawthunder.directorysearch.ui.common.DataBoundListAdapter
import com.pawthunder.directorysearch.ui.common.ItemClickCallback
import com.pawthunder.directorysearch.util.AppExecutors
import com.pawthunder.directorysearch.vo.FileDescription

/**
 * Adapter for items in dialog for choosing searched folders
 * @author Peter Grajko
 * @since 1.0
 * @property bindingComponent set to [ItemFolderDialogBinding] if fragment databinding needs to be used
 * @property clickCallback callback when item is choosed for search
 */
class FolderDialogAdapter(
    private val bindingComponent: DataBindingComponent,
    private val clickCallback: ItemClickCallback,
    appExecutors: AppExecutors
) : DataBoundListAdapter<FileDescription, ItemFolderDialogBinding>(
    appExecutors,
    object : DiffUtil.ItemCallback<FileDescription>() {

        override fun areItemsTheSame(oldItem: FileDescription, newItem: FileDescription) =
            oldItem.canonicalPath == newItem.canonicalPath

        override fun areContentsTheSame(oldItem: FileDescription, newItem: FileDescription) =
            oldItem == newItem
    }
) {

    override fun createBinding(parent: ViewGroup): ItemFolderDialogBinding =
        DataBindingUtil.inflate<ItemFolderDialogBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_folder_dialog,
            parent,
            false,
            bindingComponent
        ).apply {
            callback = clickCallback
        }

    override fun bind(binding: ItemFolderDialogBinding, item: FileDescription) {
        binding.file = item
    }
}