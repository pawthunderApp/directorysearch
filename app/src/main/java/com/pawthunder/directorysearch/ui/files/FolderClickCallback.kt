package com.pawthunder.directorysearch.ui.files

import android.view.View

/**
 * Callback when folder is clicked and need to be added into folders for searching
 * @author Peter Grajko
 * @since 1.0
 */
interface FolderClickCallback {

    /**
     * Adding new searched folder is triggered
     * @param view containing information about folder
     */
    fun addFolder(view: View)
}