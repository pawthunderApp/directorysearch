package com.pawthunder.directorysearch.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
