package com.pawthunder.directorysearch.di

import com.pawthunder.directorysearch.ui.files.FilesFragment
import com.pawthunder.directorysearch.ui.overview.OverviewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module providing fragments for activities
 * @author Peter Grajko
 * @since 1.0
 */
@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeFilesFragment(): FilesFragment

    @ContributesAndroidInjector
    abstract fun contributeOverviewFragment(): OverviewFragment
}