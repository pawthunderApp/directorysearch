package com.pawthunder.directorysearch.binding

import android.view.View
import androidx.databinding.BindingAdapter

/**
 * Adapters handling repetitive tasks in xml
 * @author Peter Grajko
 * @since 1.0
 */
object BindingAdapters {

    /**
     * Hide or show view according boolean value
     * @param view can be shown or hidden from user
     * @param show determine if [View] should be shown
     */
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }
}