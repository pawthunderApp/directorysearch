package com.pawthunder.directorysearch.vo

import androidx.room.Entity
import androidx.room.PrimaryKey

// Example of database entity
// TODO: Delete when real database entities are present
/**
 * Just example of entity
 * @property id primary key
 */
@Entity
data class ExampleEntity(
    @PrimaryKey val id: Int
)