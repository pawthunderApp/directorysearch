package com.pawthunder.directorysearch.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment

import javax.inject.Inject

/**
 * Binding adapters that work with a fragment instance.
 * @author Peter Grajko
 * @since 1.0
 * @property fragment using adapters
 */
class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {

    /**
     * Load image from url to image view through glide
     * @param imageView container for image
     * @param url where image resides and needs to be loaded
     */
    @BindingAdapter("imageUrl")
    fun bindImage(imageView: ImageView, url: String?) {
        // TODO: Uncomment if some image need be loaded from url
        //Glide.with(fragment).load(url).into(imageView)
    }
}
