package com.pawthunder.directorysearch.di

import com.pawthunder.directorysearch.service.SearchService
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Provides services ready for injection
 * @author Peter Grajko
 * @since 1.0
 */
@Suppress("unused")
@Module
abstract class ServiceModule {

    @ContributesAndroidInjector
    abstract fun contributeSearchService(): SearchService
}