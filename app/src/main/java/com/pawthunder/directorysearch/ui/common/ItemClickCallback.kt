package com.pawthunder.directorysearch.ui.common

import android.view.View

/**
 * Callback after adapter item is clicked
 * @author Peter Grajko
 * @since 1.0
 */
interface ItemClickCallback {

    /**
     * Method called when item is clicked
     * @param view clicked item container
     */
    fun itemClick(view: View)
}