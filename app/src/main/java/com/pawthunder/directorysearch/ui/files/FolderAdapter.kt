package com.pawthunder.directorysearch.ui.files

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.pawthunder.directorysearch.R
import com.pawthunder.directorysearch.databinding.ItemFolderBinding
import com.pawthunder.directorysearch.ui.common.DataBoundListAdapter
import com.pawthunder.directorysearch.util.AppExecutors
import com.pawthunder.directorysearch.vo.FileDescription

/**
 * Adapter showing folders chosen by user for searching
 * @author Peter Grajko
 * @since 1.0
 * @property bindingComponent set to [ItemFolderBinding] if fragment databinding needs to be used
 * @property clearCallback callback when folder is removed from search
 */
class FolderAdapter(
    private val bindingComponent: DataBindingComponent,
    private val clearCallback: ClearClickCallback,
    appExecutors: AppExecutors
) : DataBoundListAdapter<FileDescription, ItemFolderBinding>(
    appExecutors,
    object : DiffUtil.ItemCallback<FileDescription>() {
        override fun areItemsTheSame(oldItem: FileDescription, newItem: FileDescription) =
            oldItem.canonicalPath == newItem.canonicalPath

        override fun areContentsTheSame(oldItem: FileDescription, newItem: FileDescription) =
            oldItem == newItem
    }
) {

    override fun createBinding(parent: ViewGroup): ItemFolderBinding =
        DataBindingUtil.inflate<ItemFolderBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_folder,
            parent,
            false,
            bindingComponent
        ).apply {
            callback = clearCallback
        }

    override fun bind(binding: ItemFolderBinding, item: FileDescription) {
        binding.file = item
    }
}