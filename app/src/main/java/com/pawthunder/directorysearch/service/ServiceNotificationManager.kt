/*
 * Copyright (c) 2018 Peter Grajko. All rights reserved.
 *
 * Developed by Peter Grajko on 11/9/18 11:37 AM.
 * Last modified 11/9/18 11:37 AM.
 */

package com.pawthunder.directorysearch.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import com.pawthunder.directorysearch.R

/**
 * Manager for showing and replacing notification created through service
 * @author Peter Grajko
 * @since 1.0
 * @property service using manager for notification management
 * @constructor for manager of service notification
 */
class ServiceNotificationManager(private val service: Service) {

    /**
     * In android version oreo(API 26) and higher notification chanel is needed to show notification. Channel
     * is created and initialized.
     * @param title shown in description of channel
     * @param channelName name of channel
     * @see NotificationChannel
     */
    fun createServiceChannel(title: String, channelName: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.setSound(null, null)
            channel.description = title
            val manager = service.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
    }

    /**
     * Notification with progress bar is created and initialized
     * @param progress shown in notification progress bar
     * @param title as name of notification
     * @param text description of notification
     * @param pendingActivity activity started when notification is pressed
     * @return created and initialized notification
     */
    fun createProgressBarNotification(
        progress: Int,
        title: String?,
        text: String?,
        pendingActivity: Class<*>
    ): Notification {
        val notificationIntent = Intent(service, pendingActivity)
        val pendingIntent = PendingIntent
            .getActivity(service, 0, notificationIntent, 0)

        return initNotificationBuilder(title, text)
            .setContentIntent(pendingIntent)
            .setProgress(1000, progress, false)
            .build()
    }

    /**
     * Notification runs foreground with service and is shown to user
     * @param notification shown to user
     */
    fun startForegroundNotification(notification: Notification) {
        service.startForeground(NOTIFICATION_ID, notification)
    }

    /**
     * Notification builder is initialized with default parameters
     * @param title of notification
     * @param text description of notification
     * @return initialized builder for creating notification
     */
    private fun initNotificationBuilder(title: String?, text: String?): NotificationCompat.Builder {
        return NotificationCompat.Builder(service, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(false)
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    service.resources,
                    R.mipmap.ic_launcher
                )
            )
    }

    companion object {

        /**
         * Notification id
         */
        const val NOTIFICATION_ID: Int = 1

        /**
         * Notification channel id
         */
        const val CHANNEL_ID: String = "com.pawthunder.directorysearch.channel.notification"
    }
}