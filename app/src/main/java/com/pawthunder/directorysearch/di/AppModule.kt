package com.pawthunder.directorysearch.di

import android.app.Application
import androidx.room.Room
import com.pawthunder.directorysearch.db.ExampleDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Module provides components shared through whole app
 * @author Peter Grajko
 * @since 1.0
 * @see Module
 */
@Module(includes = [ViewModelModule::class])
class AppModule {


    @Singleton
    @Provides
    fun provideDb(app: Application) =
        Room.databaseBuilder(app, ExampleDb::class.java, "directoryDb.db")
            .addMigrations()
            .fallbackToDestructiveMigration()
            .build()

}