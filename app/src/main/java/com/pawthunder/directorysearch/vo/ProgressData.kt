package com.pawthunder.directorysearch.vo

import android.os.Parcel
import android.os.Parcelable

/**
 * Instance containing information for calculating progress
 * Class extends parcelable and can be used in bundles or intents.
 * @author Peter Grajko
 * @since 1.0
 * @property folderNumber total number of searched folders
 * @property currentFolder rank of current folder lower or equal to folder number
 * @property totalProgress total number of progress points when 100%
 * @constructor create new instance of [ProgressData]
 */
data class ProgressData(
    var folderNumber: Int,
    var currentFolder: Int,
    var totalProgress: Int
) : Parcelable {

    /**
     * Create new instance of [ProgressData] from parcel
     * @param parcel containing data
     */
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(folderNumber)
        dest?.writeInt(currentFolder)
        dest?.writeInt(totalProgress)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ProgressData> {

        override fun createFromParcel(source: Parcel): ProgressData {
            return ProgressData(source)
        }

        override fun newArray(size: Int): Array<ProgressData?> {
            return arrayOfNulls(size)
        }
    }
}