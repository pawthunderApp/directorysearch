package com.pawthunder.directorysearch.ui.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.pawthunder.directorysearch.R
import com.pawthunder.directorysearch.databinding.ItemFileBinding
import com.pawthunder.directorysearch.ui.common.DataBoundListAdapter
import com.pawthunder.directorysearch.util.AppExecutors
import com.pawthunder.directorysearch.vo.FileDescription

/**
 * Adapter showing overview of biggest files
 * @author Peter Grajko
 * @since 1.0
 * @property bindingComponent set to [ItemFileBinding] if fragment databinding needs to be used
 */
class OverviewAdapter(
    private val bindingComponent: DataBindingComponent,
    appExecutors: AppExecutors
) : DataBoundListAdapter<FileDescription, ItemFileBinding>(
    appExecutors,
    object : DiffUtil.ItemCallback<FileDescription>() {
        override fun areItemsTheSame(oldItem: FileDescription, newItem: FileDescription) =
            oldItem.canonicalPath == newItem.canonicalPath

        override fun areContentsTheSame(oldItem: FileDescription, newItem: FileDescription) =
            oldItem == newItem
    }
) {

    override fun createBinding(parent: ViewGroup): ItemFileBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_file,
            parent,
            false,
            bindingComponent
        )

    override fun bind(binding: ItemFileBinding, item: FileDescription) {
        binding.item = item
    }
}