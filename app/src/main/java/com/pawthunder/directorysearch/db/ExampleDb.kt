package com.pawthunder.directorysearch.db


import androidx.room.Database
import androidx.room.RoomDatabase
import com.pawthunder.directorysearch.vo.ExampleEntity

// Example implementation of room database
/**
 * Main database description containing version and entities. Database provide daos for database manipulation
 * @author Peter Grajko
 * @since 1.0
 */
@Database(
    entities = [ExampleEntity::class],
    version = 1,
    exportSchema = false
)
abstract class ExampleDb : RoomDatabase()
