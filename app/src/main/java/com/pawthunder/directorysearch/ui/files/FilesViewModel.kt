package com.pawthunder.directorysearch.ui.files

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.ResultReceiver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pawthunder.directorysearch.service.SearchService
import com.pawthunder.directorysearch.vo.FileDescription
import com.pawthunder.directorysearch.vo.ProgressData
import timber.log.Timber
import java.io.File
import javax.inject.Inject

/**
 * View model for files fragment handle communication with service and tasks outside UI
 * @author Peter Grajko
 * @since 1.0
 * @constructor new view model
 */
class FilesViewModel @Inject constructor() : ViewModel() {

    val itemsNumber = MutableLiveData<String>()
    val searching = MutableLiveData<Boolean>()
    val currentProgress = MutableLiveData<Int>()

    val searchedFolders = ArrayList<FileDescription>()

    var resultItems = ArrayList<FileDescription>()

    private val resultReceiver: ResultReceiver = object : ResultReceiver(Handler()) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            Timber.i("items received")

            currentProgress.value = resultData?.getInt(SearchService.NEW_PROGRESS) ?: 0

            val data = resultData?.getParcelableArrayList<FileDescription>(SearchService.SERVICE_RESULT) ?: return
            resultItems.addAll(data)
            resultItems.sortedWith(compareByDescending(FileDescription::size))

            val progressData = resultData.getParcelable<ProgressData>(SearchService.PROGRESS_DATA)

            if (progressData != null && progressData.currentFolder == progressData.folderNumber)
                searching.value = false
        }
    }

    init {
        currentProgress.value = 0
    }

    /**
     * Retrieve first level children folder from external storage and system
     * @return filde with description of folders
     */
    fun getFolders(): MutableList<FileDescription> {
        val result = ArrayList<FileDescription>()

        getDirectChildren(result, Environment.getRootDirectory())
        getDirectChildren(result, Environment.getExternalStorageDirectory())

        return result
    }

    /**
     * Searching all folders added folders for files in service
     * @param context starting service
     */
    fun findBiggestFiles(context: Context?) {
        val numSearched = searchedFolders.size

        currentProgress.value = 0
        searching.value = true

        for (i in 0 until numSearched) {
            val intent = Intent(context, SearchService::class.java)
            intent.putExtra(SearchService.SEARCHED_FOLDER, searchedFolders[i])
            intent.putExtra(SearchService.RESULT_RECEIVER, resultReceiver)
            intent.putExtra(SearchService.PROGRESS_DATA, ProgressData(numSearched, i + 1, 1000))
            context?.startService(intent)
        }
    }

    private fun getDirectChildren(list: MutableList<FileDescription>, parent: File) {
        if (parent.isDirectory) {
            for (container in parent.listFiles())
                if (container.isDirectory)
                    list.add(
                        FileDescription(
                            container.absolutePath,
                            container.canonicalPath,
                            container.name,
                            container.totalSpace
                        )
                    )
        }
    }

}