package com.pawthunder.directorysearch.service

import android.app.IntentService
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import com.pawthunder.directorysearch.R
import com.pawthunder.directorysearch.ui.MainActivity
import com.pawthunder.directorysearch.vo.FileDescription
import com.pawthunder.directorysearch.vo.ProgressData
import timber.log.Timber
import java.io.File

// TODO: rebuild to bind service for UI communication
/**
 * Service recursively searching folders for files. Service need [ResultReceiver] to communicate with UI,
 * path to root folder and [ProgressData] to show progress of search.
 * After search is done list of files is returned through receiver.
 * Service also showing notification about progress
 * @author Peter Grajko
 * @since 1.0
 */
class SearchService : IntentService("SearchService") {

    private val outputFiles = ArrayList<FileDescription>()
    private val notificationManager = ServiceNotificationManager(this)

    override fun onCreate() {
        super.onCreate()
        notificationManager.createServiceChannel(
            resources.getString(R.string.files),
            resources.getString(R.string.app_name)
        )
    }

    override fun onHandleIntent(intent: Intent?) {
        val descriptionRoot = intent?.getParcelableExtra<FileDescription>(SEARCHED_FOLDER)
        val receiver = intent?.getParcelableExtra<ResultReceiver>(RESULT_RECEIVER)
        val progressData = intent?.getParcelableExtra<ProgressData>(PROGRESS_DATA)
        val root = File(descriptionRoot?.absolutePath)

        if (progressData != null)
            showNotification(progressData)

        if (root.isDirectory)
            retrieveFiles(root)

        val bundle = Bundle()
        bundle.putParcelableArrayList(SERVICE_RESULT, outputFiles)
        bundle.putParcelable(PROGRESS_DATA, progressData)
        bundle.putInt(NEW_PROGRESS, if (progressData != null) countProgress(progressData, false) else 0)

        receiver?.send(0, bundle)
        Timber.i("items send")

        if (progressData != null && progressData.currentFolder == progressData.folderNumber)
            cancelNotification()
    }

    private fun retrieveFiles(parent: File) {
        val children = parent.listFiles()

        for (child in children ?: emptyArray()) {
            if (child.isDirectory)
                retrieveFiles(child)
            if (child.isFile)
                addFile(child)
        }
    }

    private fun addFile(file: File) {
        val fileDescription = FileDescription(
            file.absolutePath,
            file.canonicalPath,
            file.name,
            file.length()
        )
        var inserted = false

        for (i in 0 until outputFiles.size) {
            if (outputFiles[i].size < fileDescription.size) {
                outputFiles.add(i, fileDescription)
                inserted = true
                break
            }
        }

        if (!inserted)
            outputFiles.add(fileDescription)
    }

    private fun showNotification(progressData: ProgressData) {
        val progress = countProgress(progressData, true)
        val progressText = (progress / (progressData.totalProgress / 100.0)).toInt()

        val notification = notificationManager.createProgressBarNotification(
            progress,
            resources.getString(R.string.searching),
            "$progressText %",
            MainActivity::class.java
        )

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(ServiceNotificationManager.NOTIFICATION_ID, notification)
        notificationManager.startForegroundNotification(notification)
    }

    private fun cancelNotification() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(ServiceNotificationManager.NOTIFICATION_ID)
        stopForeground(true)
    }

    private fun countProgress(progressData: ProgressData, isStart: Boolean) =
        progressData.totalProgress / progressData.folderNumber * (progressData.currentFolder - if (isStart) 1 else 0)

    companion object {
        /**
         * Key for sending path of searching root folder
         */
        const val SEARCHED_FOLDER = "searchedFolder"

        /**
         * Key for sending result receiver to communicate with UI
         */
        const val RESULT_RECEIVER = "resultReceiver"

        /**
         * Key for sending files found in folders through result receiver
         */
        const val SERVICE_RESULT = "serviceResult"

        /**
         * Key for sending progress data to and from service
         */
        const val PROGRESS_DATA = "progressData"

        /**
         * Key for sending progress back to UI
         */
        const val NEW_PROGRESS = "newProgress"
    }

}
