package com.pawthunder.directorysearch.di

import com.pawthunder.directorysearch.ui.MainActivity
import com.pawthunder.directorysearch.ui.overview.OverviewActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module contributes activities with dependency injection
 * @author Peter Grajko
 * @since 1.0
 * @see Module
 */
@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeOverviewActivity(): OverviewActivity

}
