package com.pawthunder.directorysearch.ui.files

import android.view.View

/**
 * Callback when item should be removed from list
 * @author Peter Grajko
 * @since 1.0
 */
interface ClearClickCallback {

    /**
     * Triggered when clear button is clicked
     * @param view clicked button
     */
    fun clearClick(view: View)
}