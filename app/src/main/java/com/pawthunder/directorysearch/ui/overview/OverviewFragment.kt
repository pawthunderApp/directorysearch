package com.pawthunder.directorysearch.ui.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.pawthunder.directorysearch.R
import com.pawthunder.directorysearch.binding.FragmentDataBindingComponent
import com.pawthunder.directorysearch.databinding.FragmentOverviewBinding
import com.pawthunder.directorysearch.di.Injectable
import com.pawthunder.directorysearch.ui.files.FilesFragment.Companion.OUTPUT_ITEMS
import com.pawthunder.directorysearch.util.AppExecutors
import com.pawthunder.directorysearch.util.autoCleared
import com.pawthunder.directorysearch.vo.FileDescription
import javax.inject.Inject

/**
 * Fragment with overview of biggest files
 * @author Peter Grajko
 * @since 1.0
 */
class OverviewFragment : Fragment(), Injectable {

    var binding by autoCleared<FragmentOverviewBinding>()

    @Inject
    lateinit var appExecutors: AppExecutors

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_overview, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val items = activity?.intent?.getParcelableArrayListExtra<FileDescription>(OUTPUT_ITEMS) ?: ArrayList()

        binding.itemsOverview.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = OverviewAdapter(dataBindingComponent, appExecutors).apply {
                submitList(items)
            }
        }
    }
}
