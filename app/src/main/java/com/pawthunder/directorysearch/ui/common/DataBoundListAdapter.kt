package com.pawthunder.directorysearch.ui.common

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.pawthunder.directorysearch.util.AppExecutors

/**
 * Class initialize list adapter with databinding
 * @author Peter Grajko
 * @since 1.0
 */
abstract class DataBoundListAdapter<T, V : ViewDataBinding>(
    appExecutors: AppExecutors,
    diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, DataBoundHolder<V>>(
    AsyncDifferConfig.Builder<T>(diffCallback)
        .setBackgroundThreadExecutor(appExecutors.diskIO())
        .build()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBoundHolder<V> =
        DataBoundHolder(createBinding(parent))

    override fun onBindViewHolder(holder: DataBoundHolder<V>, position: Int) {
        bind(holder.binding, getItem(position))
        holder.binding.executePendingBindings()
    }

    /**
     * Call to create specific container with databinding
     * @param parent of container
     * @return initialized databinding object
     */
    protected abstract fun createBinding(parent: ViewGroup): V

    /**
     * Call for initialization of new item set to container
     * @param binding of container
     * @param item new item for container
     */
    protected abstract fun bind(binding: V, item: T)
}