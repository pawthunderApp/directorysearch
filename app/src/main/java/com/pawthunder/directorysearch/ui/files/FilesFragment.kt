package com.pawthunder.directorysearch.ui.files

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.pawthunder.directorysearch.R
import com.pawthunder.directorysearch.binding.FragmentDataBindingComponent
import com.pawthunder.directorysearch.databinding.FragmentFilesBinding
import com.pawthunder.directorysearch.di.Injectable
import com.pawthunder.directorysearch.ui.common.ItemClickCallback
import com.pawthunder.directorysearch.ui.overview.OverviewActivity
import com.pawthunder.directorysearch.util.AppExecutors
import com.pawthunder.directorysearch.util.autoCleared
import com.pawthunder.directorysearch.vo.FileDescription
import kotlinx.android.synthetic.main.dialog_list.view.*
import javax.inject.Inject

/**
 * Fragment for searching files and enable to user add search input data
 * @author Peter Grajko
 * @since 1.0
 */
class FilesFragment : Fragment(), Injectable, FolderClickCallback, ItemClickCallback, ClearClickCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var filesModel: FilesViewModel

    var binding by autoCleared<FragmentFilesBinding>()

    private var showDialog: Dialog? = null

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_files, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.clickCallback = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        filesModel = ViewModelProviders.of(this, viewModelFactory).get(FilesViewModel::class.java)

        binding.model = filesModel

        initRecycler()
        initSearch()
    }

    override fun addFolder(view: View) {
        if (activity == null) return

        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED && Build.VERSION_CODES.JELLY_BEAN < Build.VERSION.SDK_INT
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSION_READ_STORAGE
            )
        } else
            showDialog()
    }

    override fun itemClick(view: View) {
        val file = view.tag as FileDescription
        val items = filesModel.searchedFolders

        if (!items.contains(file)) {
            items.add(file)
            (binding.itemsFolder.adapter as FolderAdapter).submitList(items.clone() as MutableList<FileDescription>)
        } else
            showErrMessage(R.string.already_added_message)

        showDialog?.cancel()
    }

    override fun clearClick(view: View) {
        val items = filesModel.searchedFolders
        items.remove(view.tag as FileDescription)
        (binding.itemsFolder.adapter as FolderAdapter).submitList(items.clone() as MutableList<FileDescription>)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_READ_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    showDialog()
            }
        }
    }

    private fun initRecycler() {
        binding.itemsFolder.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = FolderAdapter(dataBindingComponent, this@FilesFragment, appExecutors)
        }
    }

    private fun initSearch() {
        filesModel.searching.value = false

        binding.filesInput.setOnEditorActionListener { view: View, actionId: Int, _: KeyEvent? ->
            if (filesModel.searchedFolders.size == 0) {
                showErrMessage(R.string.add_folders_message)
                true
            } else if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                filesModel.findBiggestFiles(activity)
                true
            } else false
        }

        filesModel.searching.observe(this, Observer { isSearching ->
            val itemsOut = filesModel.resultItems

            if (!isSearching && itemsOut.size > 0) {
                val numberItems = filesModel.itemsNumber.value?.toInt() ?: 0

                val sublist: ArrayList<FileDescription>

                if (numberItems < itemsOut.size) {
                    sublist = ArrayList()
                    for (i in 0 until numberItems)
                        sublist.add(itemsOut[i])
                } else sublist = itemsOut

                val intent = Intent(activity, OverviewActivity::class.java)
                intent.putParcelableArrayListExtra(OUTPUT_ITEMS, sublist)

                filesModel.resultItems = ArrayList()

                activity?.startActivity(intent)
            }
        })
    }

    private fun showDialog() {
        val layout = layoutInflater.inflate(R.layout.dialog_list, null)
        val items = filesModel.getFolders()

        layout.listDialogContainer.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = FolderDialogAdapter(dataBindingComponent, this@FilesFragment, appExecutors).apply {
                submitList(items)
            }
        }

        showDialog = AlertDialog.Builder(activity)
            .setTitle(R.string.folders)
            .setView(layout)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }
            .create()
        showDialog?.show()
    }

    private fun showErrMessage(messageId: Int) {
        Snackbar.make(
            binding.filesCoordinator,
            resources.getString(messageId),
            Snackbar.LENGTH_SHORT
        ).show()
    }

    companion object {
        const val PERMISSION_READ_STORAGE = 1

        const val OUTPUT_ITEMS = "outputItems"
    }
}
