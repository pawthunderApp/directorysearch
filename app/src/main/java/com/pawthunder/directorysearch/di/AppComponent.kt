package com.pawthunder.directorysearch.di

import android.app.Application
import com.pawthunder.directorysearch.DirectorySearchApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Component provides fragments, activities or services ready for dependency injection through dagger
 * Modules contains method for dependency injection of classes
 * @author Peter Grajko
 * @since 1.0
 */
@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityModule::class,
        ServiceModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: DirectorySearchApp)
}
