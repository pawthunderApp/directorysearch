package com.pawthunder.directorysearch.vo

import android.os.Parcel
import android.os.Parcelable

/**
 * Description of file containing necessary data for initialisation of file and presentation.
 * Class extends parcelable and can be used in bundles or intents.
 * @author Peter Grajko
 * @since 1.0
 * @property absolutePath absolute path of file
 * @property canonicalPath is absolute and unique for each file
 * @property name of file
 * @property size of file in bytes
 * @constructor create new instance of [FileDescription]
 */
data class FileDescription(
    val absolutePath: String?,
    val canonicalPath: String?,
    val name: String?,
    var size: Long
) : Parcelable {

    /**
     * Create new instance of [FileDescription] from parcel
     * @param parcel containing data
     */
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(absolutePath)
        dest?.writeString(canonicalPath)
        dest?.writeString(name)
        dest?.writeLong(size)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FileDescription> {

        override fun createFromParcel(source: Parcel): FileDescription {
            return FileDescription(source)
        }

        override fun newArray(size: Int): Array<FileDescription?> {
            return arrayOfNulls(size)
        }
    }
}